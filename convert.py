#!/usr/bin/env python
# from os import listdir
from os.path import isfile
from PIL import Image
import shutil
import subprocess

root_path = '/media/fred/Fred & Christina/Export/'
target_path = '/media/fred/3E0E17DE0E178E51/pics/'
MAX_WIDTH = 1440
MAX_HEIGHT = 900

import os
from os.path import join
total = 0
count = 0
for root, dirs, files in os.walk(root_path):
    target_dir = root.split(root_path)[1]
    new_target_dir = join(target_path, target_dir)
    if not os.path.exists(new_target_dir):
        print 'creating dir {}'.format(new_target_dir)
        os.makedirs(new_target_dir)

    image = None
    for source_file in files:
        try:
            full_source_file = join(root, source_file)
            statinfo = os.stat(full_source_file)
            count += 1
            width = height = 0

            target_file, extension = os.path.splitext(
                join(new_target_dir, source_file))

            # Route file types
            if extension in ['.mpg', '.mov', '.avi']:
                target_file += '.mp4'
                cmd = ['avconv', '-loglevel', 'warning', '-pre', 'slow', '-i',
                        full_source_file, target_file]
                if isfile(target_file):
                    print("Skipping {}".format(target_file))
                    continue
                return_code = subprocess.call(cmd)
                if return_code:
                    raise Exception('Bad return code {}'.format(return_code))
            elif source_file.endswith('fpbf'):
                continue
            else:  # Image file
                # Do not overwrite files
                target_file += '.webp'
                if isfile(target_file):
                    print("Skipping {}".format(target_file))
                    continue

                image = Image.open(join(root, source_file))
                width, height = image.size
                # Only run on oversized images
                if width > MAX_WIDTH:
                    wpercent = (MAX_WIDTH / float(image.size[0]))
                    hsize = int((float(image.size[1]) * float(wpercent)))

                    image = image.resize((MAX_WIDTH, hsize), Image.ANTIALIAS)
                    image.save(target_file, "WEBP", lossless=True, m=6)
                else:
                    # Just copy it across
                    shutil.copyfile(full_source_file, target_file)

        except Exception as exception:
            print 'Failed {}. Reason: {}'.format(target_file, exception)
            continue

        # Keep and print some stats
        new_statinfo = os.stat(target_file)
        savings = statinfo.st_size - new_statinfo.st_size
        total += savings
        print(
            'Converted {}({}) to {}({}) and saving {} bytes ({},{})'.format(
            full_source_file, (width, height), target_file,
            image.size if image else ('-', '-'),
            savings, int(total / 1000000), count)
            )
